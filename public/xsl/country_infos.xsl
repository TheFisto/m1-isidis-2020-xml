<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="country"/>

<xsl:template match="/">
    <tbody>
        <xsl:for-each select="//country[name=$country]">
            <h1><xsl:value-of select="name"/><span class="badge badge-light"><xsl:value-of select="name/@cc"/></span></h1>
            <hr/>
            <div class="container">
                <div class="row">
                    <div class="col-5">
                        <img src="{flag/@src}" class="img-fluid"/>
                    </div>
                    <div class="col-7">
                        <b>Captiral city: </b><xsl:value-of select="capital"/><br/><br/>
                        <b>Population: </b><xsl:value-of select="population"/> habitants<br/><br/>
                        <b>Area: </b><xsl:value-of select="area div 1000"/> km²
                    </div>
                </div>
            </div>
            <hr/>
            <iframe
                    width="100%"
                    height="400"
                    frameborder="0"
                    marginheight="0"
                    marginwidth="0"
                    src="https://maps.google.com/maps?q={name}&amp;hl=en&amp;output=embed">
            </iframe>
        </xsl:for-each>
    </tbody>
</xsl:template>

</xsl:stylesheet>
