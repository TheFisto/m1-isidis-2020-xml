<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="nom"/>
<xsl:param name="continent"/>

<xsl:template match="/">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Country</th>
        <th>Continent</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="//country">
        <xsl:sort select="name" order="ascending"/>
        <xsl:if test="(contains(name, $nom)) and (contains(@continent, $continent))">
          <tr>
            <td><img src="{flag/@src}" class="img-fluid" style="height: 1.5em;"/></td>
            <td><a href="country_infos.html?country={name}"><xsl:value-of select="name"/></a></td>
            <td><xsl:value-of select="@continent"/></td>
          </tr>
        </xsl:if>
      </xsl:for-each>
    </tbody>
  </table>
</xsl:template>

</xsl:stylesheet>