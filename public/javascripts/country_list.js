async function display() {
    //reset
    document.querySelector('#frag_country_list').innerHTML="";

    //load
    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_list.xsl');

    //params
    const nom = document.getElementById("search_name").value;
    const continent = document.getElementById("search_continent").value;

    //process
    const xsltProcessor = new XSLTProcessor();
    xsltProcessor.setParameter(null, 'nom', nom);
    xsltProcessor.setParameter(null, 'continent', continent);
    xsltProcessor.importStylesheet(xsl);
    const result = xsltProcessor.transformToFragment(xml, document);

    //display
    document.querySelector('#frag_country_list').appendChild(result);
}

display();


//EventListeners
    //searchName
document.getElementById("search_name").onkeypress=function () {
    display();
};
document.getElementById("search_name").addEventListener('keydown', function(event) {
    const key = event.key;
    if (key === "Backspace" || key === "Delete") {
        display();
    }
});
    //serchContinent
document.getElementById("search_continent").onchange=function () {
    display();
};