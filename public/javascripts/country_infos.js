async function display() {
    //load
    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_infos.xsl');

    //params
    const country = new URL(location).searchParams.get('country');

    //process
    const xsltProcessor = new XSLTProcessor();
    xsltProcessor.setParameter(null, 'country', country);
    xsltProcessor.importStylesheet(xsl);
    const result = xsltProcessor.transformToFragment(xml, document);

    //display
    document.querySelector('#frag_country_infos').appendChild(result);
}


display();
